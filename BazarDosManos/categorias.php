<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
	$d = date('H');
	if( $d < 12 ) $saudacao = "Bom dia";
	elseif ($d < 17) $saudacao = "Boa tarde";
	else  $saudacao = "Boa noite";
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Bazar Tem Tudo</title>
	</head>
	<body>
		<?php require_once("cabecalho.inc"); ?>
		<div id="corpo">
		<?php
		$pdo = new PDO("mysql:host=localhost;dbname=bazartemtudorodrigo;charset=utf8mb4","root","vertrigo");
		$statement = $pdo -> query("SELECT id_categoria,descricao FROM categoria");
		$categorias = $statement -> fetchAll();
		?>
		<?php
			foreach($categorias as $categoria){
			?>
			<tr>
			<td><?= $categoria["id_categoria"] ?></td>
			<td><?= $categoria["descricao"] ?></td>
			</tr>
			<?php
			}
			?>
		</div>

		<?php require_once("rodape.inc"); ?>

	</body>
</html>